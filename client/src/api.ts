import { AuthenticationClient } from "./generated/auth/v1/auth.client";
import { Credentials, LoginRequest, UserToken } from "./generated/auth/v1/auth";
import { PetShopClient } from "./generated/shop/v1/shop.client";
import { Pet, BuyPetRequest } from "./generated/shop/v1/shop";
import { GrpcWebFetchTransport, GrpcWebOptions } from "@protobuf-ts/grpcweb-transport";

export type { Pet } from "./generated/shop/v1/shop"

const HOST = 'http://localhost:8080';

export class AuthV1Api {
    private client: AuthenticationClient;
    private webOptions: GrpcWebOptions;

    constructor(usertoken?: string) {
        this.webOptions = {
            baseUrl: HOST,
            format: "binary"
        };
        if (usertoken !== undefined) {
            this.webOptions.meta = { 'authorization': `Bearer ${usertoken}` }
        }
        const transport = new GrpcWebFetchTransport(this.webOptions);
        this.client = new AuthenticationClient(transport);
    }

    async login(email: string, password: string): Promise<string> {
        const loginReq = LoginRequest.create({
            credentials: Credentials.toBinary({ email, password }),
        });
        const loginResponse = await this.client.login(loginReq);

        return UserToken.fromBinary(loginResponse.response.usertoken).auth;
    }

    async logout() {
        await this.client.logout({});
    }
}

export class PetShopV1Api {
    private client: PetShopClient;
    private webOptions: GrpcWebOptions;

    constructor(usertoken: string) {
        this.webOptions = {
            baseUrl: HOST,
            format: "binary",
            meta: { 'authorization': `Bearer ${usertoken}` }
        };
        const transport = new GrpcWebFetchTransport(this.webOptions);
        this.client = new PetShopClient(transport);
    }

    async getPets(): Promise<Pet[]> {
        const pets = await this.client.getPets({});
        return pets.response.pets;
    }

    async buyPet(id: number): Promise<boolean> {
        const buyPetReq = BuyPetRequest.create({ id: BigInt(id) });
        const buyPetResponse = await this.client.buyPet(buyPetReq);

        return buyPetResponse.response.success;
    }
}
