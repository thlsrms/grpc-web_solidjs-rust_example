import { createResource, For, Show } from "solid-js";
import { Pet } from "./api";
import { petShopApi } from "./signals"

export default function PetList() {
    const [pets, { refetch }] = createResource<Pet[] | undefined>(
        async () => {
            return await petShopApi()?.getPets()
        }
    )

    function buyPet(id: number) {
        petShopApi()?.buyPet(id).then((success) => {
            if (success) refetch();
        }).catch((_e) => {
            alert('Buy pet failed');
        });
    }


    return (
        <div>
            <h3>Pet List</h3>
            <Show
                when={pets()}
                fallback={<p>Fetching Pets...</p>}
                keyed>
                {(p) => (
                    <For each={p} fallback={<p>all pets are sold</p>}>
                        {(item) => {
                            return (
                                <article>
                                    <h4>{item.name}</h4>
                                    <p>{item.age} yrs</p>
                                    <button onClick={() => buyPet(Number(item.id))}>Buy</button>
                                </article>
                            )
                        }}
                    </For>
                )}
            </Show>
        </div >
    )
}
