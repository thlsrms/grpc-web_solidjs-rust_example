import { authApi, setAuthApi, setPetShopApi } from "./signals";
import { AuthV1Api, PetShopV1Api } from "./api";

export default function Login() {
    let email: HTMLInputElement | undefined;
    let password: HTMLInputElement | undefined;

    function handleSubmit(event: Event) {
        event.preventDefault();

        authApi().login(email?.value ?? '', password?.value ?? '')
            .then((response) => {
                setAuthApi(new AuthV1Api(response));
                setPetShopApi(new PetShopV1Api(response));
            })
            .catch((e) => {
                alert(`Login failed ${e}`);
            });
    }

    return (
        <div>
            <h2>Login</h2>
            <form onSubmit={handleSubmit}>
                <label for="email">
                    Email
                    <input ref={email} name="email" />
                </label>
                <label for="password">
                    Password
                    <input ref={password} name="password" type="password" />
                </label>
                <button type="submit">Login</button>
            </form>
        </div>
    )
}
