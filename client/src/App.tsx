import { Show } from "solid-js"
import { authApi, setAuthApi, setPetShopApi, petShopApi } from "./signals";
import Login from "./Login";
import PetList from "./PetList";
import { AuthV1Api } from "./api";

export default function App() {
    function logout() {
        authApi().logout().then(() => {
            setAuthApi(new AuthV1Api());
            setPetShopApi();
        })
    }

    return (
        <div class="container">
            <nav>
                <ul>
                    <li>
                        <strong>Petshop</strong>
                    </li>
                </ul>
                <ul>
                    <Show when={petShopApi()}>
                        <li>
                            <a href="#" onclick={logout}>
                                Logout
                            </a>
                        </li>
                    </Show>
                </ul>
            </nav>
            <Show when={petShopApi()} fallback={<Login />}>
                <PetList />
            </Show>
        </div>
    )
}

