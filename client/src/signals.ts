import { createSignal } from "solid-js";
import { AuthV1Api, PetShopV1Api } from "./api";

export const [authApi, setAuthApi] = createSignal(new AuthV1Api());
export const [petShopApi, setPetShopApi] = createSignal<PetShopV1Api>();
