use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Clone)]
pub struct PetDb {
    pub data: Arc<Mutex<Vec<Pet>>>,
}

#[derive(Clone)]
pub struct Pet {
    pub id: i64,
    pub age: i32,
    pub name: String,
}

// Dummy database with some data
pub fn create_pet_db() -> PetDb {
    PetDb {
        data: Arc::new(Mutex::new(vec![
            Pet {
                id: 1,
                age: 4,
                name: "Ferris".to_owned(),
            },
            Pet {
                id: 2,
                age: 3,
                name: "Lilly".to_owned(),
            },
            Pet {
                id: 3,
                age: 8,
                name: "Ratty".to_owned(),
            },
        ])),
    }
}
