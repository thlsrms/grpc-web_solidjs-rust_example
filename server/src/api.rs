use prost::{bytes::Bytes, Message};
use tonic::codegen::{CompressionEncoding, InterceptedService};
use tonic::{Request, Response, Status};

use grpc::{
    authentication_server::{Authentication, AuthenticationServer},
    pet_shop_server::{PetShop, PetShopServer},
    BuyPetRequest, BuyPetResponse, Credentials, GetPetsRequest, GetPetsResponse, LoginRequest,
    LoginResponse, LogoutRequest, LogoutResponse, Pet, UserToken,
};

/// This module includes the grpc types that tonic generates from the proto files
mod grpc {
    tonic::include_proto!("auth.v1");
    tonic::include_proto!("shop.v1");
}

pub const TOKEN: &str = "ultra_hyper_secret_token";
const AUTH_META_KEY: &str = "authorization";

#[derive(Clone)]
pub struct AuthenticationService;

#[tonic::async_trait]
impl Authentication for AuthenticationService {
    async fn login(
        &self,
        request: Request<LoginRequest>,
    ) -> Result<Response<LoginResponse>, Status> {
        let request = request.into_inner();
        let credentials = Credentials::decode(Bytes::from(request.credentials)).unwrap();

        if credentials.email == "user@email.com" && credentials.password == "password" {
            let usertoken = UserToken {
                auth: TOKEN.to_owned(),
            }
            .encode_to_vec();

            return Ok(Response::new(LoginResponse { usertoken }));
        } else {
            return Err(Status::unauthenticated("Invalid email or password"));
        }
    }

    async fn logout(
        &self,
        _request: Request<LogoutRequest>,
    ) -> Result<Response<LogoutResponse>, Status> {
        Ok(Response::new(LogoutResponse {}))
    }
}

#[derive(Clone)]
pub struct PetShopService {
    pub db: crate::pets_database::PetDb,
}

#[tonic::async_trait]
impl PetShop for PetShopService {
    async fn buy_pet(
        &self,
        request: Request<BuyPetRequest>,
    ) -> Result<Response<BuyPetResponse>, Status> {
        let mut data = self.db.data.lock().await;
        data.retain(|pet| pet.id != request.get_ref().id);

        Ok(Response::new(BuyPetResponse { success: true }))
    }

    async fn get_pets(
        &self,
        _request: Request<GetPetsRequest>,
    ) -> Result<Response<GetPetsResponse>, Status> {
        let data = self.db.data.lock().await;
        Ok(Response::new(GetPetsResponse {
            pets: data
                .iter()
                .map(|pet| Pet {
                    id: pet.id,
                    age: pet.age,
                    name: pet.name.clone(),
                })
                .collect(),
        }))
    }
}

type InterceptorFunction = fn(Request<()>) -> Result<Request<()>, Status>;

pub fn initialize_authentication_service() -> AuthenticationServer<AuthenticationService> {
    AuthenticationServer::new(AuthenticationService)
        .accept_compressed(CompressionEncoding::Gzip)
        .send_compressed(CompressionEncoding::Gzip)
}

pub fn initialize_petshop_service(
) -> InterceptedService<PetShopServer<PetShopService>, InterceptorFunction> {
    InterceptedService::new(
        PetShopServer::new(PetShopService {
            db: crate::pets_database::create_pet_db(),
        })
        .accept_compressed(CompressionEncoding::Gzip)
        .send_compressed(CompressionEncoding::Gzip),
        check_auth_meta,
    )
}

fn check_auth_meta(request: Request<()>) -> Result<Request<()>, Status> {
    let meta = request.metadata();
    if let Some(authentication) = meta.get(AUTH_META_KEY) {
        if authentication == format!("Bearer {TOKEN}").as_str() {
            return Ok(request);
        } else {
            return Err(Status::unauthenticated("Bad authorization token"));
        };
    }
    Err(Status::unauthenticated("Not athorization meta given"))
}
