use axum::routing::{any_service, get_service};
use std::{
    convert::Infallible,
    net::{IpAddr, Ipv6Addr, SocketAddr},
};
use tonic_web::enable;
use tower_http::{
    compression::CompressionLayer, services::ServeDir, trace::TraceLayer,
    validate_request::ValidateRequestHeaderLayer,
};

mod api;
mod pets_database;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    std::env::set_var("RUST_LOG", "debug,hyper=info,mio=info");
    tracing_subscriber::fmt::init();
    const PORT: u16 = 8080;
    run(&SocketAddr::new(IpAddr::V6(Ipv6Addr::LOCALHOST), PORT)).await
}

async fn run(addr: &SocketAddr) -> Result<(), Box<dyn std::error::Error>> {
    tracing::info!("Serving on http://{}", addr);

    let app_router = axum::Router::new()
        .route(
            "/auth.v1.Authentication/*rpc",
            any_service(enable(api::initialize_authentication_service()))
                .layer(TraceLayer::new_for_grpc()),
        )
        .route(
            "/shop.v1.PetShop/*rpc",
            any_service(enable(api::initialize_petshop_service()))
                .layer::<_, _, Infallible>(ValidateRequestHeaderLayer::bearer(api::TOKEN))
                .layer::<_, _, Infallible>(TraceLayer::new_for_grpc()),
        )
        .nest_service(
            "/",
            get_service(ServeDir::new("dist"))
                .layer::<_, _, Infallible>(TraceLayer::new_for_http())
                .layer::<_, _, Infallible>(CompressionLayer::new().gzip(true)),
        )
        .nest_service(
            "/assets",
            get_service(ServeDir::new("dist/assets")).layer(CompressionLayer::new().gzip(true)),
        );

    axum::Server::bind(addr)
        .serve(app_router.into_make_service())
        .await?;

    Ok(())
}
