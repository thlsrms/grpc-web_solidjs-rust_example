fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure().compile(
        &[
            "../proto/authapi/auth/v1/auth.proto",
            "../proto/shopapi/shop/v1/shop.proto",
        ],
        &["../proto"],
    )?;
    Ok(())
}
