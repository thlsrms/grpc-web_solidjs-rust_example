### gRPC web Client & Server
This is an example of a **gRPC WEB** client built with [solidjs](https://github.com/solidjs/solid) and [@protobuf-ts](https://github.com/timostamm/protobuf-ts) and server running on [Axum](https://github.com/tokio-rs/axum) using [Tonic](https://github.com/hyperium/tonic).
<br>
<br>
The updated example using axum also serves the client, so it can comunicate with the server using the same port.

<br>

To generate the client stubs, [buf CLI](https://github.com/bufbuild/buf#installation) needs to be installed.


#### Running the example
cd into the **client** folder and run `yarn build` then copy the `dist` folder into the server folder to be served.
<br>
cd into the **server** folder and run `cargo run` or `cargo watch -x run`.

<br>

To login using the hard-coded user in this example: `user@email.com` & `password`
<hr>
<br>

To only generate the client's protobuf stubs run `buf generate` in the root of the project.

