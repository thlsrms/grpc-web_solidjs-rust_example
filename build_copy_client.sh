#!/bin/bash
cd $(dirname -- "$0";)/client && \
    yarn build && \
    cp -r dist ../server && \
    cd $(dirname -- "$0";)/server
echo "Done";

